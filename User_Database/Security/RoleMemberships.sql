﻿ALTER ROLE [db_owner] ADD MEMBER [shakti];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [shakti];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [shakti];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [shakti];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [shakti];


GO
ALTER ROLE [db_datareader] ADD MEMBER [shakti];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [shakti];


GO
ALTER ROLE [db_denydatareader] ADD MEMBER [shakti];


GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [shakti];

