﻿CREATE TABLE [dbo].[UserData] (
    [UserId]    UNIQUEIDENTIFIER CONSTRAINT [DF_UserData_UserID] DEFAULT (newid()) NOT NULL,
    [UserName]  VARCHAR (50)     NOT NULL,
    [Email]     VARCHAR (50)     NOT NULL,
    [DOB]       DATE             NULL,
    [IsActive]  BIT              DEFAULT ((1)) NULL,
    [IsDeleted] BIT              DEFAULT ((0)) NULL,
    [CreatedOn] DATETIME         DEFAULT (getutcdate()) NULL,
    [CreatedBy] UNIQUEIDENTIFIER NULL,
    [UpdatedOn] DATETIME         NULL,
    [UpdatedBy] UNIQUEIDENTIFIER NULL,
    [DeletedOn] DATETIME         NULL,
    [DeletedBy] UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

