﻿
CREATE PROCEDURE [dbo].[DeleteUser]
(
	@UserId UNIQUEIDENTIFIER,
	@DeletedBy UNIQUEIDENTIFIER
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM [dbo].[UserData] WITH(NOLOCK) WHERE UserId = @UserId AND IsDeleted = 0)
		BEGIN 
			UPDATE [dbo].[UserData]
			SET IsDeleted = 1, IsActive = 0, DeletedOn = GETUTCDATE(), DeletedBy = @DeletedBy
			WHERE UserId = @UserId

			SELECT 'Data Deleted' AS 'Message'
		END

	ELSE
		BEGIN
			SELECT 'Invalid Id' AS 'Message'
		END
    
END
