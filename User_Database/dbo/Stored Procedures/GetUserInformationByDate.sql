﻿--EXEC GetUserInformationByDate '2023/05/08'


CREATE PROCEDURE GetUserInformationByDate
    @DateOnly Date
AS
BEGIN
    SELECT *
    FROM [dbo].[UserData]
    WHERE CONVERT(DATE, CreatedOn AT TIME ZONE 'UTC') = @DateOnly
END




