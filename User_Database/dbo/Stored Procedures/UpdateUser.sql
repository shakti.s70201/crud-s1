﻿CREATE PROCEDURE [dbo].[UpdateUser]
(
	@UserId	UNIQUEIDENTIFIER,
	@UserName VARCHAR(50),
	@Email VARCHAR(50),
	@DOB VARCHAR(20), 
	@OperatedBy UNIQUEIDENTIFIER
)

AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM [dbo].[UserData] WITH(NOLOCK) WHERE UserId = @UserId AND IsDeleted = 0)
		BEGIN 
			
			
			
			UPDATE [dbo].[UserData]
			SET Email = IIF(@Email ='' ,Email,@Email), UserName = TRIM(IIF(@UserName ='' ,UserName,@UserName)),
			DOB = IIF(@DOB ='' ,DOB ,@DOB), UpdatedOn = GETUTCDATE(),UpdatedBy = @OperatedBy
			WHERE UserId = @UserId

			SELECT 1 AS Status, 'User data have been updated successfully' AS 'Message'

		END
	ELSE 
		BEGIN
			SELECT 'Inavlid UserId' AS 'Message'
		END
END
