﻿
--exec [dbo].[InsertUser] 'C293FCFC-83A0-4681-B934-6070928BFA58','xyz','xyzses@yopmai11.com','','3fa85f64-5717-4562-b3fc-2c963f66afa6'
--cast(NULLIF([@DOB],'') AS DATE)

CREATE PROCEDURE [dbo].[InsertUser]
(
	@UserId	UNIQUEIDENTIFIER,
	@UserName VARCHAR(50),
	@Email VARCHAR(50),
	@DOB VARCHAR(20), 
	@OperatedBy UNIQUEIDENTIFIER
)

AS
BEGIN
	SET NOCOUNT ON;
	
	SET @DOB = IIF(@DOB ='' ,Null ,@DOB)

	IF NOT EXISTS(SELECT 1 FROM [dbo].[UserData] WITH(NOLOCK) WHERE Email = @Email AND IsDeleted = 0)
		BEGIN
			INSERT INTO [dbo].[UserData] (UserID, UserName, Email, DOB,CreatedOn, CreatedBy)
			VALUES (NEWID(), TRIM(@UserName), @Email,@DOB , GETUTCDATE(), @OperatedBy )

			SELECT 1 AS Status, 'User data inserted' AS 'Message'
		END
	ELSE
		BEGIN
			SELECT @Email + ' EMAIL ALREADY EXISTS' AS 'Message'
		END
END
