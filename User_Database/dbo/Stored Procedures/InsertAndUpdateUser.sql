﻿CREATE PROCEDURE [dbo].[InsertAndUpdateUser]
(
	@UserId	VARCHAR(50),
	@UserName VARCHAR(50),
	@Email VARCHAR(50),
	@DOB VARCHAR(20), 
	@OperatedBy UNIQUEIDENTIFIER
)

AS
BEGIN
	SET NOCOUNT ON;
	IF (@UserId = NULL)
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM [dbo].[tblUserData] WITH(NOLOCK) WHERE Email = @Email AND IsDeleted = 0)
				BEGIN
					INSERT INTO [dbo].[tblUserData] (UserID, UserName, Email, DateOfBirth,CreatedOn, CreatedBy)
					VALUES (NEWID(), TRIM(@UserName), @Email, @DOB, GETUTCDATE(), @OperatedBy )

					SELECT 'User data inserted' AS 'Message'
				END
			ELSE
				BEGIN
					SELECT @Email + ' EMAIL ALREADY EXISTS' AS 'Message'
				END
		END
	ELSE
		BEGIN
			IF EXISTS(SELECT 1 FROM [dbo].[tblUserData] WITH(NOLOCK) WHERE UserID = @UserId AND IsDeleted = 0)
				BEGIN 
					UPDATE [dbo].[tblUserData] 
					SET Email = @Email, UserName = @UserName, DateOfBirth = @DOB, UpdatedOn = GETUTCDATE(),UpdatedBy = @OperatedBy
					WHERE UserID = @UserId

					SELECT 'User data have been updated successfully' AS 'Message'

				END
			ELSE 
				BEGIN
					SELECT 'Inavlid UserId' AS 'Message'
				END
		END
END