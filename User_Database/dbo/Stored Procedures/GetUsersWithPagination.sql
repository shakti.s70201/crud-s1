﻿--ALTER PROCEDURE [dbo].[GetUsersWithPagination]
--(
--    @Start int,
--    @SortColumn varchar(50),
--    @PageSize int,
--    @SearchKey varchar(50)
--)
--AS
--BEGIN
--    SET NOCOUNT ON;

--    SELECT UserId, UserName, Email, DOB, IsActive, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, DeletedOn, DeletedBy
--    FROM (
--        SELECT ROW_NUMBER() OVER (ORDER BY CASE WHEN @SortColumn = 'UserName' THEN UserName ELSE Email END) AS RowNum, *
--        FROM UserData
--        WHERE (@SearchKey IS NULL OR UserName LIKE '%' + @SearchKey + '%' OR Email LIKE '%' + @SearchKey + '%')
--            AND IsDeleted = 0
--    ) AS UserDataWithRowNum
--    WHERE RowNum BETWEEN (@Start - 1) * @PageSize + 1 AND @Start * @PageSize;
--END

--EXEC [dbo].[GetUsersWithPagination] @Start = 0, @PageSize = 0, @SortColumn = '', @SearchKey = ''
CREATE PROCEDURE [dbo].[GetUsersWithPagination]
(
    @Start int = 0,
    @SortColumn varchar(50),
    @PageSize int = 0,
    @SearchKey varchar(50)
)
AS
BEGIN
	BEGIN TRY
	
		SET NOCOUNT ON;
		Select 1 AS Status, 'Data Found' AS Message

		--DECLARE @Message VARCHAR;
		DECLARE @RowCount BIGINT
		SELECT @RowCount = COUNT(1) 
		FROM [dbo].[UserData] WITH (NOLOCK)

		SET @PageSize = IIF(@PageSize = 0,@RowCount,@PageSize)

		SELECT UserId, UserName, Email, DOB, IsActive, IsDeleted, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, DeletedOn, DeletedBy
		FROM [dbo].[UserData] WITH (NOLOCK)
		WHERE (@SearchKey IS NULL OR UserName LIKE '%' + @SearchKey + '%' OR Email LIKE '%' + @SearchKey + '%')
		AND IsDeleted = 0
		ORDER BY CASE WHEN @SortColumn = 'UserName' THEN UserName ELSE CreatedOn END
		OFFSET @Start * @PageSize ROWS
		FETCH NEXT @PageSize ROWS ONLY;

		--SELECT @RowCount AS 'TotalRows';
	
	END TRY   
	BEGIN CATCH 
		
        DECLARE @Message VARCHAR = Error_message();  
        DECLARE @ErrorSeverity INT = Error_severity();  
        DECLARE @ErrorState INT = Error_state();    
        RAISERROR(@Message, @ErrorSeverity, @ErrorState);  
	END CATCH  

	SELECT 0 Status, @Message Message
END
