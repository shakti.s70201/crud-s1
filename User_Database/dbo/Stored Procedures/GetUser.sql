﻿CREATE PROCEDURE [dbo].[GetUser]
(
	@UserId UNIQUEIDENTIFIER
)

AS 
BEGIN
	
	--IF NOT EXISTS (SELECT 1 FROM [dbo].[UserData] WITH(NOLOCK) WHERE UserId = @UserId AND IsActive = 1)
 --       BEGIN
 --           RAISERROR('Provided Id does not exists please try again.',16, 1)

 --       END
			
	--ELSE 
	--	BEGIN
	--		SELECT TOP 1 UserId, UserName, Email, DOB AS Data
	--		FROM [dbo].[UserData] WITH(NOLOCK) WHERE UserId = @UserId
	--		return;
			 
	--	END 

	IF EXISTS(SELECT 1 FROM [dbo].[UserData] WITH(NOLOCK) WHERE UserId = @UserId)
		BEGIN
			SELECT 1 AS Status, 'Data found' AS Message

			SELECT TOP 1 UserId, UserName, Email, CONVERT(VARCHAR, DOB, 102) AS DOB , IsActive
			FROM [dbo].[UserData] WITH(NOLOCK) WHERE UserId = @UserId 
			 
		END 
	ELSE 
		BEGIN
			SELECT 1 AS Status, 'Given ID is dose not exists' AS 'Message'
			SELECT NULL AS UserRecord
		END
END