﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActiveDeactive]
	@UserId UNIQUEIDENTIFIER,
	@Status BIT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT 1 FROM [dbo].[UserData] WHERE UserId = @UserId AND IsDeleted = 0)
		BEGIN
			UPDATE [dbo].[UserData]
			SET IsActive =  @Status 
			WHERE UserId = @UserId

			SELECT 'Active has been updated.' AS Message
		END
	ELSE
		BEGIN
			SELECT 'Provided UserId Not Found' AS Message
		END
END
