﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Insert
    {
        public Guid? UserId { get; set; }

        [Required, StringLength(120, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\' ]+$", ErrorMessage = "Name should not contain numbers and special characters")]
        public string? UserName { get; set; }


        [Required]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Not an valid Email Address.")]
        [EmailAddress]
        public string? Email { get; set; }

        //public  DateOfBirth  { get; set; }
        [Required]
        // [DataType(DataType.Date)]
        //[RegularExpression("^(\\d{2}).(\\d{2}).(\\d{4})$")]
        [RegularExpression("(0?[1-9]|[12]\\d|30|31)[^\\w\\d\\r\\n:](0?[1-9]|1[0-2])[^\\w\\d\\r\\n:](\\d{4}|\\d{2})$")]
        //[RegularExpression("@(((0|1)[0-9]|2[0-9]|3[0-1])\\/(0[1-9]|1[0-2])\\/((19|20)\\d\\d))$")]
        public string? DOB { get; set; }
        //public DateOnly DateOfBirth { get; set; }

        public Guid? OperatedBy { get; set; }
    }
}
