﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ActiveDeactive
    {
        public Guid UserId { get; set; }
        public bool Status { get; set; }
    }
}
