﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ResponseList
    {
        public bool Status { get; set; }
        public string? Message { get; set; }
        public List<UserRecord>? Data { get; set; }
    }
    public class UserRecord
    {
        public Guid UserId { get; set; }
        public string? UserName { get; set; }
        public string? Email { get; set; }
        public string? DOB { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string? CreatedOn { get; set; }
        public Guid? CreatedBy { get; set; }
        public string? UpdatedOn { get; set; }
        public Guid? UpdatedBy { get; set; }
        public string? DeletedOn { get; set; }
        public Guid? DeletedBy { get; set; }

    }
}
