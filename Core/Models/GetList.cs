﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class GetList
    {
        public int Start { get; set; }
        //public int End { get; set; }
        public int PageSize { get; set; }
        public string? SortColumn { get; set; }
        public string? SearchKey { get; set; }

    }
}
