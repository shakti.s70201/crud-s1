﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Delete
    {
        public Guid UserId { get; set; }

        public Guid DeletedBy { get; set; }
        
    }
}
