﻿using Core.Models;
using static Core.Models.GetUserSingle;

namespace Core.Interface
{
    public interface IUserRepository
    {
        Task<Response> Add(Insert insert);

        Task<Response> Update(Update update);

        Task<Response> Delete(Delete delete);

        Task<Response> Active(ActiveDeactive activeDeactive);

        Task<ResponseList> Get(GetUserSingle getUserSingle);

        Task<ResponseList> GetList(GetList getList);

    }
}