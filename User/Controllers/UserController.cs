﻿using Core.Interface;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;
using static Core.Models.GetUserSingle;

namespace User.Controllers
{
    [Route("user")]
    // CHanges temp
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;


        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }



        private async Task<ActionResult<T>> HandleInvalidModelState<T>(Func<Task<T>> action)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                .SelectMany(x => x.Errors)
                .Select(x => x.ErrorMessage)),
                });
            }

            T response = await action();

            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        private async Task<ActionResult<T>> HandleException<T>(Func<Task<T>> action)
        {
            try
            {
                return await HandleInvalidModelState(action);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }



        // -----------      Action Methods      ----------------


        [HttpPost("insert")]
        public async Task<IActionResult> Add([FromBody] Insert insert)
        {
            //var response = await _userRepository.Add(insert);

            return Ok(await HandleException(async () => await _userRepository.Add(insert)));
        }

        [HttpPut("update/{UserId}")]
        public async Task<IActionResult> Update([FromBody] Update update)
        {
            //var response = await _userRepository.Update(update);

            return Ok(await HandleException(async () => await _userRepository.Update(update)));
        }

        [HttpPatch("delete")]
        public async Task<IActionResult> Delete([FromBody] Delete delete)
        {
            //var response = await _userRepository.Delete(delete);

            return Ok(await HandleException(async () => await _userRepository.Delete(delete)));
        }


        [HttpPut("setActive/{UserId}")]
        public async Task<IActionResult> Active(ActiveDeactive activeDeactive)
        {
            return Ok(await HandleException(async () => await _userRepository.Active(activeDeactive)));
        }


        

        [HttpGet("get/{UserId}")]
        public async Task<ActionResult<ResponseList>> GetUser(GetUserSingle getUserSingle)
        {
            return Ok(await HandleException(async () => await HandleException(async () => await _userRepository.Get(getUserSingle))));
        }

        [HttpGet("getUserList")]
        public async Task<ActionResult<ResponseList>> GetUserList(GetList getList)
        {
            return await HandleException(async () => await _userRepository.GetList(getList));
        }

        //[HttpGet("EmailSender")]
        //public async Task<IActionResult> SendEmail()
        //{
        //    await EmailService.EmailSender();
        //    return Ok();
        //}

    }
}
