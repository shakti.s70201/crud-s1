﻿using Core.Interface;
using Core.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using static Core.Models.GetUserSingle;

namespace Infrastructure
{
    public class UserRepository : IUserRepository
    {

        private readonly IConfiguration _configuration;

        private static string _connectionString = string.Empty;

        private readonly IEmailNotifier _notifier;



        //Constructor
        public UserRepository(IConfiguration configuration, IEmailNotifier emailNotifier)
        {
            _configuration = configuration;
            _connectionString = _configuration["ConnectionStrings:UserInformation"]!;

            _notifier = emailNotifier;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }




        //---------    Methods     ---------

        public async Task<Response> Add(Insert insert)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[InsertUser]", insert, commandType: CommandType.StoredProcedure).ConfigureAwait(false);
            if (response.Status)
            {
                await _notifier.EmailSender(insert.Email!, $"Hello New User {insert.UserName}");
            }
            return response;
        }


        public async Task<Response> Update(Update update)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[UpdateUser]", update, commandType: CommandType.StoredProcedure).ConfigureAwait(false);

            return response;
        }


        public async Task<Response> Delete(Delete delete)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[DeleteUser]", delete, commandType: CommandType.StoredProcedure).ConfigureAwait(false);

            return response;
        }


        public async Task<Response> Active(ActiveDeactive activeDeactive)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[ActiveDeactive]", activeDeactive, commandType: CommandType.StoredProcedure);
            return response;
        }


        public async Task<ResponseList> Get(GetUserSingle getUserSingle)
        {
            ResponseList response;
            using IDbConnection db = Connection;

            var result = await db.QueryMultipleAsync("[GetUser]", getUserSingle, commandType: CommandType.StoredProcedure);
            response = result.Read<ResponseList>().First();
            response.Data = new() { result.Read<UserRecord>().First()};
            
            return response;
        }

        public async Task<ResponseList> GetList(GetList getList)
        {
            ResponseList response;
            using IDbConnection db = Connection;

            var result = await db.QueryMultipleAsync("[GetUsersWithPagination]", getList, commandType: CommandType.StoredProcedure);
            response = result.Read<ResponseList>().FirstOrDefault()!;
            response.Data = result.Read<UserRecord>().ToList();
            return response;

        }
    }
}